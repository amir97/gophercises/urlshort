module gitlab.com/amir97/gophercises/urlshort

go 1.13

require (
	github.com/DennisVis/urlshort v0.0.0-20180819174551-927cdd178e5b
	github.com/boltdb/bolt v1.3.1
	github.com/gophercises/urlshort v0.0.0-20190723121003-cc800dbaf411
	github.com/syndtr/goleveldb v1.0.0
	golang.org/x/sys v0.0.0-20200409092240-59c9f1ba88fa // indirect
	gopkg.in/yaml.v2 v2.2.8
)
